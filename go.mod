module bitbucket.org/uwaploe/acmcal

go 1.13

require (
	bitbucket.org/uwaploe/tsfpga v0.7.1
	github.com/charmbracelet/bubbles v0.15.0
	github.com/charmbracelet/bubbletea v0.24.0
	github.com/charmbracelet/lipgloss v0.6.0
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	google.golang.org/grpc v1.45.0
)
