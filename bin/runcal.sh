#!/usr/bin/env bash
#
# Run the ACM compass calibration process on the DPC
#

# Get DPC ID from the hostname
host="$(hostname)"
id=$(printf "%03d" "${host##*-}")

caldir="$HOME/COMPASSCAL-${id}"
mkdir -p $caldir

cd $caldir
acmcal /dev/ttyS1
