reset
set size square
set xrange [-1:1]
set yrange [-1:1]
set grid
set xlabel "HY"
set ylabel "HX"
set title 'Deep Profiler ACM Compass Correction'
plot 'mag.txt' u 2:1 w p title 'uncorrected', '' u 4:3 w p title 'corrected'
