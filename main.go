// Acmcal provides a CLI to run an ACM compass calibration procedure
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"time"

	"bitbucket.org/uwaploe/acmcal/internal/ema"
	"bitbucket.org/uwaploe/acmcal/internal/power"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/tarm/serial"
	"google.golang.org/grpc"
)

const Usage = `Usage: acmcal [options] serialdev

Run a compass calibration process on the ACM attached to serialdev.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	acmBaud int    = 19200
	filtLen int    = 10
	rpcAddr string = "localhost:10101"
	pwrDio  string = "8160_LCD_D1"
	noPower bool
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.IntVar(&acmBaud, "baud", acmBaud, "Serial interface baud rate")
	flag.IntVar(&filtLen, "filter", filtLen, "Number of points in averaging filter")
	flag.StringVar(&rpcAddr, "rpc-addr", rpcAddr,
		"host:port for power control gRPC server")
	flag.StringVar(&pwrDio, "dio", pwrDio, "power control DIO line")
	flag.BoolVar(&noPower, "no-power", noPower, "do not switch the ACM power")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func grpcConnect(addr string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())

	return grpc.Dial(addr, opts...)
}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	if !noPower && pwrDio != "" {
		gconn, err := grpcConnect("localhost:10101")
		if err != nil {
			log.Fatalf("Cannot connect to TS-FPGA server: %v", err)
		}

		sw := power.NewDio(gconn, pwrDio)
		// Power-on the ACM
		sw.On()
		defer sw.Off()
	}

	port, err := serial.OpenPort(&serial.Config{
		Name:        args[0],
		Baud:        acmBaud,
		ReadTimeout: time.Second * 5,
	})
	if err != nil {
		log.Fatalf("Cannot open port %q: %v", args[0], err)
	}

	cwd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	store, err := NewStore(cwd)
	if err != nil {
		log.Fatalf("Cannot open data files: %v", err)
	}
	defer store.Close()

	p := tea.NewProgram(
		initialModel(port, ema.NewFilter(2./float64(filtLen+1), 3), store),
		tea.WithAltScreen())

	if _, err := p.Run(); err != nil {
		log.Println(err)
	}
}
