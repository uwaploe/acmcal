package main

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/uwaploe/acmcal/internal/ema"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

const timeFormat = "2006-01-02 15:04:05"

type modelStyles struct {
	labelStyle lipgloss.Style
	textStyle  lipgloss.Style
	hdrStyle   lipgloss.Style
}

func (m modelStyles) renderValue(key, val string) string {
	return lipgloss.JoinHorizontal(lipgloss.Top, m.labelStyle.Render(key),
		m.textStyle.Render(val))
}

type model struct {
	ch        chan dataRecord
	textInput textinput.Model
	rdr       io.Reader
	filt      *ema.VecEma
	store     Store
	rec       dataRecord
	response  string
	modelStyles
}

func initialModel(rdr io.Reader, filt *ema.VecEma, store Store) model {
	ti := textinput.New()
	// Disable input until first data record arrives
	ti.Blur()
	ti.CharLimit = 156
	ti.Width = 20

	m := model{
		textInput: ti,
		rdr:       rdr,
		ch:        make(chan dataRecord, 1),
		filt:      filt,
		store:     store,
	}
	m.modelStyles.labelStyle = lipgloss.NewStyle().Bold(true).Width(30)
	m.modelStyles.textStyle = lipgloss.NewStyle().PaddingLeft(4)
	m.modelStyles.hdrStyle = lipgloss.NewStyle().
		MarginLeft(1).
		MarginRight(5).
		MarginBottom(1).
		Foreground(lipgloss.Color("#FFD000")).
		Background(lipgloss.Color("#6124DF")).
		Padding(0, 1)

	return m
}

func listenForData(r io.Reader, ch chan dataRecord) tea.Cmd {
	return func() tea.Msg {
		scanner := bufio.NewScanner(r)
		for scanner.Scan() {
			rec := dataRecord{}
			rec.T = time.Now()
			if err := rec.UnmarshalText(scanner.Bytes()); err != nil {
				continue
			}
			select {
			case ch <- rec:
			default:
			}
		}

		return scanner.Err()
	}
}

func waitForData(ch chan dataRecord) tea.Cmd {
	return func() tea.Msg {
		return <-ch
	}
}

func (m model) Init() tea.Cmd {
	return tea.Batch(listenForData(m.rdr, m.ch),
		waitForData(m.ch))
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case dataRecord:
		if !m.textInput.Focused() {
			m.textInput.Focus()
		}
		m.store.AddRecord(msg)
		m.filt.Update(msg.Mag[:])
		m.rec = msg
		return m, waitForData(m.ch)
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "esc":
			return m, tea.Quit
		case "enter":
			x, err := strconv.ParseFloat(m.textInput.Value(), 64)
			if err == nil {
				m.textInput.Reset()
				copy(m.rec.Mag[:], m.filt.State())
				m.store.AddMark(m.rec, x)
				m.response = "heading logged"
			} else {
				m.response = err.Error()
			}
		}
	}
	m.textInput, cmd = m.textInput.Update(msg)
	return m, cmd
}

func (m model) View() string {
	var b strings.Builder
	b.WriteString(m.hdrStyle.Render("DP ACM Compass Calibration") + "\n")
	b.WriteString(m.renderValue("Time:", m.rec.T.Format(timeFormat)) + "\n")
	b.WriteString(m.renderValue("Mag. Flux:", fmt.Sprintf("%.3f", m.rec.Mag)) + "\n")
	b.WriteString(m.renderValue("Tilt:", fmt.Sprintf("%.2f", m.rec.Tilt)) + "\n")
	b.WriteString("\n\nEnter vehicle heading: " + m.textInput.View())
	b.WriteString("\n\n" + m.response)
	b.WriteString("\n\n esc/ctrl+c: quit\n")
	return b.String()
}
