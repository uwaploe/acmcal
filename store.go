package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"time"
)

const TFORMAT = "2006-01-02 15:04:05.99-0700"

type Store struct {
	data, marks io.WriteCloser
}

func NewStore(dir string) (Store, error) {
	var (
		st  Store
		err error
	)

	t := time.Now()
	st.data, err = os.Create(filepath.Join(dir,
		fmt.Sprintf("acmcal_%s_data.csv", t.Format("20060102T150405"))))
	if err != nil {
		return st, err
	}
	st.marks, err = os.Create(filepath.Join(dir,
		fmt.Sprintf("acmcal_%s_marks.csv", t.Format("20060102T150405"))))
	return st, err
}

func (st Store) Close() {
	st.data.Close()
	st.marks.Close()
}

func (st Store) AddRecord(rec dataRecord) error {
	_, err := fmt.Fprintf(st.data,
		"%s,%.3f,%.3f,%.3f,%.2f,%.2f\n",
		rec.T.Format(TFORMAT),
		rec.Mag[0], rec.Mag[1], rec.Mag[2],
		rec.Tilt[0], rec.Tilt[1])
	return err
}

func (st Store) AddMark(rec dataRecord, heading float64) error {
	_, err := fmt.Fprintf(st.marks,
		"%s,%.3f,%.3f,%.3f,%.1f,%.2f,%.2f\n",
		rec.T.Format(TFORMAT),
		rec.Mag[0], rec.Mag[1], rec.Mag[2], heading,
		rec.Tilt[0], rec.Tilt[1])
	return err
}
