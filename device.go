package main

import (
	"bytes"
	"fmt"
	"time"
)

type dataRecord struct {
	T    time.Time
	Tilt [2]float64
	Mag  [3]float64
	Vel  [4]float64
}

func (rec *dataRecord) UnmarshalText(text []byte) error {
	_, err := fmt.Fscanf(bytes.NewReader(text),
		" %f, %f, %f, %f, %f, %f, %f, %f, %f",
		&rec.Tilt[0], &rec.Tilt[1],
		&rec.Mag[0], &rec.Mag[1], &rec.Mag[2],
		&rec.Vel[0], &rec.Vel[1], &rec.Vel[2], &rec.Vel[3])
	return err
}
