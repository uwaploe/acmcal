package main

import "testing"

const INPUT = "  -0.27,   -3.63, -0.0962, -0.5913, -0.8007,   11.90,   -4.20,    0.63,   -5.84\r\n"

var OUTPUT = dataRecord{
	Tilt: [2]float64{-0.27, -3.63},
	Mag:  [3]float64{-0.0962, -0.5913, -0.8007},
	Vel:  [4]float64{11.90, -4.20, 0.63, -5.84},
}

func TestRecParse(t *testing.T) {
	rec := &dataRecord{}
	err := rec.UnmarshalText([]byte(INPUT))
	if err != nil {
		t.Fatal(err)
	}

	if *rec != OUTPUT {
		t.Errorf("Bad output; expected %#v, got %#v", OUTPUT, rec)
	}
}
