// Ema implements an exponential moving average filter for vector input
package ema

import (
	"fmt"
	"sync"
)

type VecEma struct {
	alpha, beta float64
	n           int
	mu          *sync.RWMutex
	state       []float64
}

// NewFilter returns a new EMA filter for vector data. Alpha is the weighting
// coefficient and dim is the size of the input vector.
func NewFilter(alpha float64, dim int) *VecEma {
	f := new(VecEma)
	f.n = dim
	f.alpha = alpha
	f.beta = 1. - alpha
	f.mu = &sync.RWMutex{}
	return f
}

// Update takes a new input vector and returns a new output vector.
func (f *VecEma) Update(x []float64) ([]float64, error) {
	if len(x) != f.n {
		return nil, fmt.Errorf("Bad input size: %d != %d", len(x), f.n)
	}

	if f.state == nil {
		f.state = make([]float64, f.n)
		copy(f.state, x)
	} else {
		f.mu.Lock()
		for i, v := range x {
			f.state[i] = f.alpha*v + f.beta*f.state[i]
		}
		f.mu.Unlock()
	}
	return f.state, nil
}

func (f *VecEma) State() []float64 {
	f.mu.RLock()
	x := f.state
	f.mu.RUnlock()
	return x
}
