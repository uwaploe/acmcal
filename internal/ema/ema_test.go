package ema

import (
	"reflect"
	"testing"
)

func TestBasic(t *testing.T) {
	filter := NewFilter(1., 3)

	x := []float64{1., 2., 3.}
	result, err := filter.Update(x)

	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}

	result, err = filter.Update(x)
	if !reflect.DeepEqual(result, x) {
		t.Errorf("Bad result: want %v, got %v", x, result)
	}

	x = []float64{1.}
	result, err = filter.Update(x)
	if err == nil {
		t.Errorf("Bad input not detected")
	}
}
